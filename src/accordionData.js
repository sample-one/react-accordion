

const accordionData = [
  {
    id: 1,
    title: "What is React?",
    content: "React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies."
  },
  {
    id: 2,
    title: "What is Tailwind CSS?",
    content: "Tailwind CSS is a utility-first CSS framework that provides low-level utility classes to build custom designs without writing custom CSS."
  },
  {
    id: 3,
    title: "What is Vite?",
    content: "Vite is a frontend build tool that aims to provide a faster and leaner development experience for modern web projects. It consists of two major parts: a dev server that provides rich feature enhancements over native ES modules, and a build command that bundles your code with Rollup, pre-configured to output highly optimized static assets for production."
  },
  {
    id: 4,
    title: "How to use an accordion component?",
    content: "An accordion component is used to display collapsible content panels for presenting information in a limited amount of space. Users can click on a section title to expand or collapse its content."
  },
  {
    id: 5,
    title: "Benefits of using Vite with React",
    content: "Using Vite with React provides a number of benefits including faster development server start times, instant hot module replacement (HMR), optimized build output, and a leaner project setup."
  }
];

export default accordionData;

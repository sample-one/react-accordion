import React, { useState } from 'react'
import accordionData from '../accordionData'


function Accordion({accordionData}) {
  
  const [accordionIds , setAccordionIds] = useState([])


  //handler function

  const handlerAccordion = (newID)=>{
    setAccordionIds((prevAccID) =>{
      return accordionIds.includes(newID) ? accordionIds.filter((acc_id) => acc_id !== newID) :[...prevAccID, newID]
    })
  }



   console.log(accordionIds,accordionIds)
  return (

     <>
       <div className='bg-red-100'>

       {
        //object destructuring

       accordionData && accordionData.map(   ({id,title,content}) => {

        return(

          
          <div className='  container mx-auto p-4 w-6/12 ' key={id} >

          <div className=' mb-6 border-b-1 bg-gradient-to-r from-purple-400 via-pink-500 to-red-500  p-4 rounded-lg shadow-sm mt-12' >
  
           <div className='accordion-title  cursor-pointer bg-gray-200 p-4 hover:bg-gray-300 transition-colors duration-300 flex justify-between' onClick={() =>handlerAccordion(id)}>
             <h3 className='text-lg font-semibold text-black'>{title}</h3>
             <span className="text-xl font-bold ml-auto">+</span>
           </div>
            
              {

                accordionIds?.map((acc_id,index)=>(
                 acc_id===id && (
                                  <div className='accordion-description bg-gray-100  p-4 text-black'>
                                     <p>{content}</p>
                                  </div>  
                 )

                ))
              
              }     
            
  
          </div>  
      
         </div>


        )
       })

      }



       </div>

       
       
     
  </>

  )
}

export default Accordion
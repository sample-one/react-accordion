import { useState } from 'react'
import Accordion from './components/accordion'
import accordionData from './accordionData'
import './App.css'

function App() {
  

  return (
    <> 
      
      <Accordion  accordionData = {accordionData}/>
    </>
  )
}

export default App
